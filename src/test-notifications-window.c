/* test-notifications-window.c
 *
 * Copyright 2022 Cakeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "test-notifications-window.h"

struct _TestNotificationsWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkButton           *button;
  GtkLabel            *label;

  guint                clicked_notifications;
};

G_DEFINE_FINAL_TYPE (TestNotificationsWindow, test_notifications_window, GTK_TYPE_APPLICATION_WINDOW)

static void
on_button_clicked_cb (TestNotificationsWindow *self,
                      GtkButton               *button)
{
  g_autoptr(GNotification) notification = NULL;

  g_assert (TEST_NOTIFICATIONS_IS_WINDOW (self));
  g_assert (button != NULL);

  notification = g_notification_new ("Hi there");
  g_notification_set_body (notification, "Click me to increment the count!");
  g_notification_set_default_action (notification, "app.notification-clicked");

  g_application_send_notification (g_application_get_default (), "default", notification);
}

static void
test_notifications_window_class_init (TestNotificationsWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gtk/TestNotifications/test-notifications-window.ui");
  gtk_widget_class_bind_template_child (widget_class, TestNotificationsWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, TestNotificationsWindow, button);
  gtk_widget_class_bind_template_child (widget_class, TestNotificationsWindow, label);
  gtk_widget_class_bind_template_callback (widget_class, on_button_clicked_cb);
}

static void
test_notifications_window_init (TestNotificationsWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

void
test_notifications_window_notification_clicked (TestNotificationsWindow *self)
{
  g_return_if_fail (TEST_NOTIFICATIONS_IS_WINDOW (self));

  self->clicked_notifications++;
  gtk_label_set_label (self->label, g_strdup_printf ("Clicked Notifications: %u", self->clicked_notifications));

  gtk_window_present (GTK_WINDOW (self));
  // gtk_window_present (GTK_WINDOW (self));
}

